import React from 'react';
import '../App.css';

class newselement extends React.Component {
    constructor(props){
        super(props)
        this.onDelete = this.onDelete.bind(this)
    }
    onDelete(){
        const{onDelete,Headline} = this.props;
        onDelete(Headline)
    }
render(){
    const {Headline , Description ,Author ,Date} =this.props;
    
    
    
 
  return (
    <div className="container">

          
             <table className="table tr td th  text-center">
                        <thead>
                      
                            <tr>
                            <th>{Headline}</th>
                                <th>{Description}</th>
                                <th>{Author}</th>
                                
                                <th>{Date}</th> 
                                <th><button onClick={this.onDelete}>Delete</button></th>
                               
                            </tr>
                           
                        </thead>
                       

                    </table>
        
      

    </div>
  );
}

}
export default newselement;
