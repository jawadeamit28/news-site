import React from 'react';
import DatePicker from "react-datepicker";
import '../App.css';

import "react-datepicker/dist/react-datepicker.css";
class addnews extends React.Component {
    constructor(props){
        super(props)
        this.onSubmit = this.onSubmit.bind(this)
        const date = new Date();
        const isoDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000));
        this.state = {
            startDate: isoDate
          };
        this.handleChange = this.handleChange.bind(this)
    }
    onSubmit(event){
        event.preventDefault();
        const date = this.state.startDate.toLocaleDateString();
        console.log(this.state.startDate.toLocaleDateString());
     //   const date = (JSON.stringify(this.state.startDate)).slice(1,11)
     // const date = this.state.startDate
     // console.log(date)
       
       this.props.onAdd(this.headline.value,this.description.value,this.author.value,date)
       this.headline.value=''
       this.description.value=''
       this.author.value=''
       this.setState({
           startDate:''
       })
    //   console.log(this.state.startDate)
    }
    handleChange = date => {
      console.log(date);
        this.setState({
          startDate: date
        });
      };
render(){
    const mystyle = {
        color: "black",
        margin: "8px 0",
        padding: "20px",
        fontFamily: "Arial",
        padding: "12px 50px"
      };
  return (
   <div classname="input s" >
<h4 >Add News</h4>
<form classname="input s" style={mystyle} onSubmit={this.onSubmit}>
<input type='text'style={mystyle} placeholder="HeadLines" ref={headline => this.headline = headline} required/>
<input type='text' style={mystyle} placeholder="Description" ref={description => this.description = description} required/>
<input type='text' style={mystyle} placeholder="Author" ref={author => this.author = author} required/>
<DatePicker
utcOffset={0}
       selected={this.state.startDate}
       onChange={ this.handleChange }
       
       name="startDate"
       dateFormat="yyyy-MM-dd"
       timeFormat={false} 
       required
      />
<button style={mystyle}>Add News</button>
<hr/>
</form>
    </div>
  );
}

}
export default addnews;
