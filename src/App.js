import React from 'react';
import logo from './logo.svg';
import './App.css';
import Newselement from './component/newselement'
import Addnews from './component/addNews'
const news=[
  {  
  Headline:'',
  Description:'',
  Author:'',
  Date:''
  }

]
localStorage.setItem('news',JSON.stringify(news))
class App extends React.Component {
constructor(props){
  super(props);
  this.state={
    news:JSON.parse(localStorage.getItem('news'))
  };
  this.onDelete = this.onDelete.bind(this);
  this.onAdd = this.onAdd.bind(this);
}
 componentWillMount(){
   const news = this.getNews()
   this.setState({news})
 }

 getNews(){
  return this.state.news
  
 }

 onAdd(Headline,Description,Author,Date){
   const news = this.getNews();
  let ness = news.reverse()
  console.log(ness)
   news.push({
     Headline,
     Description,
     Author,
     Date
   })
   this.setState({news : ness.sort((a,b)=>a.Date > b.Date)})

 }
 onDelete(Headline){
   const news = this.getNews();
   const filternews = news.filter(news=>{
     return news.Headline != Headline;
   })

 this.setState({news : filternews})
 }
  render(){
    const mystyle = {
      color: "white",
      backgroundColor: "DodgerBlue",
      padding: "10px",
      fontFamily: "Arial"
    };
    return (
    <div className="s ">
      <h1 style={mystyle}>News App</h1>
      <Addnews onAdd = {this.onAdd}/>
      {
        this.state.news.map(news=>{
          if(news.Headline != ''){
          return(
            <Newselement 
            key={news.Headline}
            {...news}
            onDelete={this.onDelete}/>
          )
        }})
      }
    </div>
  );
}

}
export default App;
